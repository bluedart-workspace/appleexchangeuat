package com.capiot.appleexchange.processors;


import com.jayway.jsonpath.JsonPath;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;
import java.util.Map;

public class BlitExchangeProcessor {

    RestTemplate restTemplate = new RestTemplate();

    private static final Logger LOG = LoggerFactory.getLogger(BlitExchangeProcessor.class);


    public void processBlitExchange(Exchange exchange) {

        ProducerTemplate template = exchange.getContext().createProducerTemplate();
        JSONObject jsonObject = new JSONObject((LinkedHashMap) exchange.getIn().getBody());
        exchange.getIn().setHeader("CCRCRDREF", JsonPath.parse(exchange.getIn().getBody()).read("$._source.quote.items[0].id"));
        Map<String, Object> stringHeader = exchange.getIn().getHeaders();
        exchange.getIn().setHeader("cawbno_id", JsonPath.parse(exchange.getIn().getBody()).read("$._source.quote.items[0].tracking_information.outbound.tracking_number"));

        exchange.getIn().setHeader("CacheKey", JsonPath.parse(exchange.getIn().getBody()).read("$._source.quote.routed_vendor_location"));
        exchange.getIn().setHeader("CacheName", "AddressStatusMaster");
        template.setDefaultEndpointUri("direct:ExchangeBlitHeaderSetter");
        template.sendBodyAndHeaders(jsonObject, stringHeader);
        LOG.info("After send template", jsonObject.toString());
    }

    public void getCode(Exchange exchange) {

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.valueOf("text/xml; charset=utf-8"));
        httpHeaders.set("SOAPAction", String.valueOf(exchange.getIn().getHeader("SOAPAction")));
        String request = String.valueOf(exchange.getIn().getBody());
        HttpEntity entity = new HttpEntity<>(request, httpHeaders);
        ResponseEntity<String> response = restTemplate.exchange((String) exchange.getIn().getHeader("CamelHttpuri"), HttpMethod.POST, entity, String.class);
        String responseString = response.getBody();
        exchange.getIn().setBody(responseString);

    }


}
