package com.capiot.appleexchange.processors;

import com.capiot.appleexchange.roesources.IgniteCacheResource;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

public class CacheProcessor {

    private static final Logger logger = LoggerFactory.getLogger(CacheProcessor.class);

    RestTemplate restTemplate = new RestTemplate();

    public void getCache(Exchange exchange) {

        IgniteCacheResource igniteCacheResource = new IgniteCacheResource();
        igniteCacheResource.setCacheKey(String.valueOf(exchange.getIn().getHeader("Cachekey")));
        igniteCacheResource.setCacheName(String.valueOf(exchange.getIn().getHeader("CacheName")));
        igniteCacheResource.setCacheOperation("Get");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity entity = new HttpEntity<>(igniteCacheResource, httpHeaders);
        ResponseEntity<String> responseEntity = restTemplate.exchange((String) exchange.getIn().getHeader("igniteClientURI"), HttpMethod.POST, entity, String.class);
        exchange.getIn().setHeader("CacheValue", String.valueOf(responseEntity.getBody()));
        logger.info(String.valueOf(responseEntity));
        logger.info("Cache Received : ${headers}, ${body}");


    }


}
