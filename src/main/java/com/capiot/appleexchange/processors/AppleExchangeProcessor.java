package com.capiot.appleexchange.processors;

import org.apache.camel.Exchange;
import org.apache.camel.language.bean.Bean;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Map;


public class AppleExchangeProcessor {

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    private static final Logger LOG = LoggerFactory.getLogger(AppleExchangeProcessor.class);

    public void process(Exchange exchange) throws Exception {

        String CacheValue = (String) exchange.getIn().getHeader("CacheValue");
        exchange.getIn().setHeader("CCUSTADR1", CacheValue.split("_")[3]);
        exchange.getIn().setHeader("CCNEEADR2", CacheValue.split("_")[4]);
        exchange.getIn().setHeader("CCNEEADR3", CacheValue.split("_")[5]);
        exchange.getIn().setHeader("CCUSTCODE", CacheValue.split("_")[1]);
        exchange.getIn().setHeader("CCUSTNAME", CacheValue.split("_")[2]);
        exchange.getIn().setHeader("CCUSTPIN", CacheValue.split("_")[6]);
        exchange.getIn().setHeader("CCUSTTEL", CacheValue.split("_")[7]);
        exchange.getIn().setHeader("CPACKTYPE", CacheValue.split("_")[10]);
        exchange.getIn().setHeader("CMODE", CacheValue.split("_")[11]);
        exchange.getIn().setHeader("CSENDER", CacheValue.split("_")[12]);
        exchange.getIn().setHeader("CORGAREA", CacheValue.split("_")[0]);
        exchange.getIn().setHeader("CRETURNID", String.valueOf(CacheValue.split("_")[8]));
        String headers = String.valueOf(exchange.getIn().getHeaders());
        LOG.info("After Cache setter : ", headers);

    }


    public void removeHeader(Exchange exchange) {
        exchange.getIn().removeHeader("BlitQuery");
        exchange.getIn().removeHeader("SOAPAction");
        exchange.getIn().removeHeader("RS_CORGSCRCD");
        exchange.getIn().removeHeader("RS_CORGAREA");
        exchange.getIn().removeHeader("NDCLRDVAL");
        exchange.getIn().removeHeader("ReverseC2pcTimeStamp");
        exchange.getIn().removeHeader("MappingXSLT");
        exchange.getIn().removeHeader("LastUpdated");
        exchange.getIn().removeHeader("GetScCodeXslt");
        exchange.getIn().removeHeader("firedTime");
        exchange.getIn().removeHeader("Error");
        exchange.getIn().removeHeader("CUSTADR2");
        exchange.getIn().removeHeader("CCUSTPIN");
        exchange.getIn().removeHeader("CCUSTNAME");
        exchange.getIn().removeHeader("CCUSTMOB");
        exchange.getIn().removeHeader("CCUSTADR1");
        exchange.getIn().removeHeader("CCNEETEL");
        exchange.getIn().removeHeader("CCNEEPIN");
        exchange.getIn().removeHeader("CCNEENAME");
        exchange.getIn().removeHeader("CCNEEADR3");
        exchange.getIn().removeHeader("CCNEEADR2");
        exchange.getIn().removeHeader("CCNEEADR1");
        exchange.getIn().removeHeader("CATTENTION");
        exchange.getIn().removeHeader("CamelHttpResponseText");
        exchange.getIn().removeHeader("CamelHttpResponseCode");
        exchange.getIn().removeHeader("CacheValue");
        exchange.getIn().removeHeader("CacheOperation");
        exchange.getIn().removeHeader("CacheName");
        exchange.getIn().removeHeader("Cachekey");
        exchange.getIn().removeHeader("access-control-allow-credentials");
        exchange.getIn().removeHeader("STATDATETIME");
        exchange.getIn().removeHeader("CEXCHAWB");
        exchange.getIn().removeHeader("CCRCRDREF");
        exchange.getIn().removeHeader("TruckSort");
        exchange.getIn().removeHeader("X-Powered-By");
        exchange.getIn().removeHeader("TruckLane");
        exchange.getIn().removeHeader("TradeInFlag");
        exchange.getIn().removeHeader("StatusResponse");
        exchange.getIn().removeHeader("sqlQuery");
        exchange.getIn().removeHeader("ShipQueue");
        exchange.getIn().removeHeader("ServiceLevel");
        exchange.getIn().removeHeader("Server");
        exchange.getIn().removeHeader("SendRequest");
        exchange.getIn().removeHeaders("rabbitmq.*");
        exchange.getIn().removeHeader("FrwdC2pcTimeStamp");
        exchange.getIn().removeHeader("found");
        exchange.getIn().removeHeader("firedTime");
        exchange.getIn().removeHeader("Date");
        exchange.getIn().removeHeader("CSENDER");
        exchange.getIn().removeHeader("CRTOTEL");
        exchange.getIn().removeHeader("CRTOPIN");
        exchange.getIn().removeHeaders("CRTOADR*");
        exchange.getIn().removeHeader("CRETURNID");
        exchange.getIn().removeHeader("CPACKTYPE");
        exchange.getIn().removeHeader("CORGAREA");
        exchange.getIn().removeHeader("Content-Length");
        exchange.getIn().removeHeader("CODCode");
        exchange.getIn().removeHeader("CMODE");
        exchange.getIn().removeHeader("CCUSTTEL");
        exchange.getIn().removeHeader("CCUSTPIN");
        exchange.getIn().removeHeader("CCUSTNAME");
        exchange.getIn().removeHeader("CCUSTCODE");
        exchange.getIn().removeHeaders("CCUSTADR*");
        exchange.getIn().removeHeaders("CCNEEADR*");
        exchange.getIn().removeHeader("CamelSqlRowCount");
        exchange.getIn().removeHeader("CamelHttpResponseText");
        exchange.getIn().removeHeader("CamelHttpResponseCode");
        exchange.getIn().removeHeader("CamelHttpMethod");
        exchange.getIn().removeHeader("AppleQuery");
        exchange.getIn().removeHeader("AppleForward");
        exchange.getIn().removeHeader("AddressId");
        exchange.getIn().removeHeader("CamelHttpUri");


    }


    public void updateRequest(Exchange exchange) {

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        String request = exchange.getIn().getBody(String.class);
        HttpEntity entity = new HttpEntity<>(request, httpHeaders);
        ResponseEntity responseEntity = restTemplate.exchange((String) exchange.getIn().getHeader("CamelHttpuri"), HttpMethod.POST, entity, String.class);

        Map headers = exchange.getIn().getHeaders();
        LOG.info("Updated request : ", headers.toString());
    }


}
