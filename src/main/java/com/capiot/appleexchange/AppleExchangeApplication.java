package com.capiot.appleexchange;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppleExchangeApplication {


    private static final Logger logger = LoggerFactory.getLogger(AppleExchangeApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(AppleExchangeApplication.class, args);

    }


}
