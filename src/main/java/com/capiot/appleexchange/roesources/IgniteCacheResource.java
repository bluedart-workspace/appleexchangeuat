package com.capiot.appleexchange.roesources;


public class IgniteCacheResource {

    private String cacheKey;
    private String cacheName;
    private String cacheValue = "";
    private String cacheOperation;
    private Integer timeToLive = 0;

    public IgniteCacheResource() {
    }

    public String getCacheKey() {
        return cacheKey;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }

    public String getCacheName() {
        return cacheName;
    }

    public void setCacheName(String cacheName) {
        this.cacheName = cacheName;
    }

    public String getCacheValue() {
        return cacheValue;
    }

    public void setCacheValue(String cacheValue) {
        this.cacheValue = cacheValue;
    }

    public String getCacheOperation() {
        return cacheOperation;
    }

    public void setCacheOperation(String cacheOperation) {
        this.cacheOperation = cacheOperation;
    }

    public Integer getTimeToLive() {
        return timeToLive;
    }

    public void setTimeToLive(Integer timeToLive) {
        this.timeToLive = timeToLive;
    }

    @Override
    public String toString() {
        return "IgniteCacheResource{" +
                "cacheKey='" + cacheKey + '\'' +
                ", cacheName='" + cacheName + '\'' +
                ", cacheValue='" + cacheValue + '\'' +
                ", cacheOperation='" + cacheOperation + '\'' +
                ", timeToLive='" + timeToLive + '\'' +
                '}';
    }
}
