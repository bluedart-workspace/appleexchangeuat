package com.capiot.appleexchange.controllers;


import org.apache.camel.ProducerTemplate;
import org.apache.camel.spring.boot.CamelContextConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@RestController
public class CamelController {

    private static final Logger logger = LoggerFactory.getLogger(CamelController.class);

    @Autowired
    ProducerTemplate producerTemplate;

    @RequestMapping
    public void startCamel(){
        producerTemplate.sendBody("direct:firstRoute", "Calling via SpringBoot Rest Controller");


    }

    @RequestMapping(value = "/controlbus", method = RequestMethod.GET)
    public ResponseEntity<String> controlBus(@RequestParam String routeId, @RequestParam String action) {

        String response = "";
        String endpointURI = "";
        switch (action.toLowerCase()) {
            case "start":
                endpointURI = "controlbus:route?routeId=" + routeId + "&action=start";
                logger.debug("In Start Route");
                logger.debug(endpointURI);
                producerTemplate.sendBody(endpointURI, null);
                response = routeId + " action " + action + " completed";
                break;
            case "stop":
                endpointURI = "controlbus:route?routeId=" + routeId + "&action=stop";
                logger.debug("In Stop Route");
                logger.debug(endpointURI);
                producerTemplate.sendBody(endpointURI, null);
                response = routeId + " action " + action + " completed";
                break;
            case "status":
                endpointURI = "controlbus:route?routeId=" + routeId + "&action=status";
                logger.debug("In Status Route");
                logger.debug(endpointURI);
                String status = producerTemplate.requestBody(endpointURI, null, String.class);
                response = routeId + " action " + action + " completed with response : " + status;
                break;
            case "stats":
                endpointURI = "controlbus:route?routeId=" + routeId + "&action=stats";
                logger.debug("In Stats Route");
                logger.debug(endpointURI);
                response = producerTemplate.requestBody(endpointURI, null, String.class);
//                response = routeId + " action " + action + " completed";

                break;
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
