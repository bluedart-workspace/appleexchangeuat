package com.capiot.appleexchange.config;

import com.rabbitmq.client.ConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.http.HttpClientConfigurer;
import org.apache.camel.component.http.HttpComponent;
import org.apache.camel.spring.boot.CamelContextConfiguration;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class MyAppConfig {

    private static final Logger logger = LoggerFactory.getLogger(MyAppConfig.class);
    @Autowired
    @Qualifier("myDS")
    BasicDataSource dataSource;

    @Value("jdbc.datasource.password")
    private String sqlPassword;

    @Value("jdbc.datasource.username")
    private String sqlUserName;

    @Value("jdbc.datasource.url")
    private String databaseUrl;

    @Value("jdbc.datasource.driver-class-name")
    private String driverClassName;

    @Autowired
    private PoolingHttpClientConnectionManager poolingConnectionManager;
    @Autowired
    private ConnectionKeepAliveStrategy connectionKeepAliveStrategy;



//
//    @Bean(name = "myDS")
//    public DataSource myDataSource() {
//
//        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
//        dataSourceBuilder.driverClassName("oracle.jdbc.OracleDriver");
//        dataSourceBuilder.url("jdbc:oracle:thin:@172.18.62.11:1521:bluetest");
//        dataSourceBuilder.username("ediuser");
//        dataSourceBuilder.password("ediuser");
//        return dataSourceBuilder.build();
//    }


    @Bean(name = "myDS")
    public BasicDataSource myDataSource() {

        BasicDataSource ds = new BasicDataSource();
        ds.setUrl("jdbc:oracle:thin:@172.18.62.11:1521:bluetest");
        ds.setDriverClassName("oracle.jdbc.OracleDriver");
        ds.setUsername("ediuser");
        ds.setPassword("ediuser");
        ds.setMaxActive(2);
        ds.setMaxIdle(5);
        ds.setMaxWait(10000);
        return ds;

    }

    @Bean
    public ConnectionFactory rabbitConnectionFactory() {
        ConnectionFactory factory = new ConnectionFactory();
//        factory.setHost("localhost");
//        factory.setHost("172.18.111.121");
        factory.setHost("172.18.1.250");
        factory.setPort(5672);
        factory.setVirtualHost("/");
        factory.setUsername("bluedart");
        factory.setPassword("bluedart");
        factory.setAutomaticRecoveryEnabled(true);
        factory.setTopologyRecoveryEnabled(true);

        return factory;
    }

    @Bean(name = "restTemplate")
    public RestTemplate restTemplate() {
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(100);
        connectionManager.setDefaultMaxPerRoute(20);

        RequestConfig requestConfig = RequestConfig
                .custom()
                .setConnectionRequestTimeout(5000) // timeout to get connection from pool
                .setSocketTimeout(5000) // standard connection timeout
                .setConnectTimeout(5000) // standard connection timeout
                .build();

        HttpClient httpClient = HttpClientBuilder.create()
                .setConnectionManager(connectionManager)
                .setDefaultRequestConfig(requestConfig).build();

        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);


        return new RestTemplate(requestFactory);
    }


    @Bean
    CamelContextConfiguration camelContextConfiguration() {
        return new CamelContextConfiguration() {
            @Override
            public void beforeApplicationStart(CamelContext camelContext) {

                HttpComponent httpComponent = camelContext.getComponent("http", HttpComponent.class);
                httpComponent.setHttpClientConfigurer(new HttpClientConfigurer() {
                    @Override
                    public void configureHttpClient(HttpClientBuilder clientBuilder) {
//                        clientBuilder.setSSLSocketFactory(sslContext);
//                        RegistryBuilder.<ConnectionSocketFactory>create().register("http", sslContext).build();
                        clientBuilder.setConnectionManager(poolingConnectionManager);
                        clientBuilder.setKeepAliveStrategy(connectionKeepAliveStrategy);
                        clientBuilder.setConnectionReuseStrategy(DefaultConnectionReuseStrategy.INSTANCE);

                    }
                });
                camelContext.setMessageHistory(true);
                camelContext.setUseMDCLogging(true);

                camelContext.getRegistry().bind("myDS", dataSource);
            }

            @Override
            public void afterApplicationStart(CamelContext camelContext) {

            }
        };


    }

}
