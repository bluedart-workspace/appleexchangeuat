<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:param name="CCUSTPIN"/>
    <xsl:template match="/">
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                          xmlns:tem="http://tempuri.org/"
                          xmlns:bd="http://schemas.datacontract.org/2004/07/BD.Entities.Common"
                          xmlns:bd1="http://schemas.datacontract.org/2004/07/BD.Entities.Area">
            <soapenv:Header/>
            <soapenv:Body>
                <!--<tem:GetStateCodeForPin>-->
                <tem:GetStateCodeForPin>
                    <!-- This is Consignee Pincode from Apple Fwd-->
                    <tem:pincode><xsl:value-of select="$CCUSTPIN"/></tem:pincode>
                </tem:GetStateCodeForPin>
                <!--</tem:GetStateCodeForPin>-->
            </soapenv:Body>
        </soapenv:Envelope>
    </xsl:template>
</xsl:stylesheet>
