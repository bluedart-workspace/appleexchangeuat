<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!--Customer Data From Forward Manifest -->
    <xsl:param name="CCUSTADR1"/>
    <xsl:param name="CCUSTADR2"/>
    <xsl:param name="CCUSTADR3"/>
    <xsl:param name="CCUSTNAME"/>
    <xsl:param name="CCUSTPIN"/>
    <xsl:param name="CCUSTMOB"/>
    <!--Consignee Data From DB -->
    <xsl:param name="CATTENTION"/>
    <xsl:param name="CCNEEADR1"/>
    <xsl:param name="CCNEEADR2"/>
    <xsl:param name="CCNEEADR3"/>
    <xsl:param name="CCNEENAME"/>
    <xsl:param name="CCNEEPIN"/>
    <xsl:param name="CCNEETEL"/>
    <!--Exchange amt from fwd manifest-->
    <xsl:param name="NDCLRDVAL"/>

    <xsl:param name="CBTPAREA">BOM</xsl:param>
    <xsl:param name="CBTPCODE">412801</xsl:param>
    <xsl:param name="CBPTCODE_KN">427674</xsl:param>
    <xsl:param name="RS_CORGAREA"/>
    <xsl:param name="RS_CORGSCRCD"/>

    <!-- Exchange Get StateCode -->
    <xsl:param name="RS_State"/>

    <xsl:template match="/">
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/"
                          xmlns:bd="http://schemas.datacontract.org/2004/07/BD.Entities.Common"
                          xmlns:bd1="http://schemas.datacontract.org/2004/07/BD.Entities.Area">
            <soapenv:Header/>
            <soapenv:Body>
                <tem:WaybillGeneration_Ext>
                    <tem:req>
                        <!--Cashify-->
                        <bd1:CATTENTION>
                            <xsl:value-of select="substring($CATTENTION,0,31)"/>
                        </bd1:CATTENTION>

                        <!-- <xsl:choose>
                            <xsl:when test="$RS_CORGAREA = $CBTPAREA">
                                <bd1:CBTPAREA></bd1:CBTPAREA>
                                <bd1:CBTPCODE></bd1:CBTPCODE>
                            </xsl:when>
                            <xsl:otherwise>
                                <bd1:CBTPAREA><xsl:value-of select="$CBTPAREA"/></bd1:CBTPAREA>
                                <bd1:CBTPCODE><xsl:value-of select="$CBTPCODE"/></bd1:CBTPCODE>
                            </xsl:otherwise>
                        </xsl:choose> -->
                        <xsl:choose>
                            <xsl:when test='$RS_State = "KN"'>
                                <bd1:CBTPAREA>
                                    <xsl:value-of select="$CBTPAREA"/>
                                </bd1:CBTPAREA>
                                <bd1:CBTPCODE>
                                    <xsl:value-of select="$CBPTCODE_KN"/>
                                </bd1:CBTPCODE>
                            </xsl:when>
                            <xsl:otherwise>
                                <bd1:CBTPAREA>
                                    <xsl:value-of select="$CBTPAREA"/>
                                </bd1:CBTPAREA>
                                <bd1:CBTPCODE>
                                    <xsl:value-of select="$CBTPCODE"/>
                                </bd1:CBTPCODE>
                            </xsl:otherwise>
                        </xsl:choose>
                        <bd1:CCMDTYCODE>014</bd1:CCMDTYCODE>
                        <bd1:CCNEEADR1>
                            <xsl:value-of select="substring($CCNEEADR1,0,31)"/>
                        </bd1:CCNEEADR1>
                        <bd1:CCNEEADR2>
                            <xsl:value-of select="substring($CCNEEADR2,0,31)"/>
                        </bd1:CCNEEADR2>
                        <bd1:CCNEEADR3>
                            <xsl:value-of select="substring($CCNEEADR3,0,31)"/>
                        </bd1:CCNEEADR3>
                        <bd1:CCNEEMOB></bd1:CCNEEMOB>
                        <bd1:CCNEENAME>
                            <xsl:value-of select="substring($CCNEENAME,0,31)"/>
                        </bd1:CCNEENAME>
                        <bd1:CCNEEPIN>
                            <xsl:value-of select="$CCNEEPIN"/>
                        </bd1:CCNEEPIN>
                        <bd1:CCNEETEL>
                            <xsl:value-of select="$CCNEETEL"/>
                        </bd1:CCNEETEL>
                        <bd1:CCRCRDREF>
                            <xsl:value-of select="/LinkedHashMap/quote/items/id"/>
                        </bd1:CCRCRDREF>

                        <!--Consignee from Forward Manifest-->
                        <bd1:CCUSTADR1>
                            <xsl:value-of select="substring($CCUSTADR1,0,31)"/>
                        </bd1:CCUSTADR1>
                        <bd1:CCUSTADR2>
                            <xsl:value-of select="substring($CCUSTADR2,0,31)"/>
                        </bd1:CCUSTADR2>
                        <bd1:CCUSTADR3>
                            <xsl:value-of select="substring($CCUSTADR3,0,31)"/>
                        </bd1:CCUSTADR3>

                        <!--This is harcoded for now:-->
                        <!-- <xsl:choose>
                            <xsl:when test="$RS_CORGAREA = $CBTPAREA">
                                <bd1:CCUSTCODE><xsl:value-of select="$CBTPCODE"/></bd1:CCUSTCODE>
                            </xsl:when>
                            <xsl:otherwise>
                                <bd1:CCUSTCODE>112733</bd1:CCUSTCODE>
                            </xsl:otherwise>
                        </xsl:choose> -->
                        <bd1:CCUSTCODE>112733</bd1:CCUSTCODE>
                        <bd1:CCUSTMOB>
                            <xsl:value-of select="$CCUSTMOB"/>
                        </bd1:CCUSTMOB>
                        <bd1:CCUSTNAME>
                            <xsl:value-of select="substring($CCUSTNAME,0,31)"/>
                        </bd1:CCUSTNAME>
                        <bd1:CCUSTPIN>
                            <xsl:value-of select="$CCUSTPIN"/>
                        </bd1:CCUSTPIN>

                        <bd1:CCUSTTEL></bd1:CCUSTTEL>
                        <bd1:CMODE>P</bd1:CMODE>
                        <bd1:CMRKSNOS1></bd1:CMRKSNOS1>
                        <bd1:CMRKSNOS2></bd1:CMRKSNOS2>
                        <bd1:CMRKSNOS3></bd1:CMRKSNOS3>


                        <!--Harcoded For Now:-->
                        <bd1:COFFCLTIME>1800</bd1:COFFCLTIME>
                        <bd1:CORGAREA>
                            <xsl:value-of select="$RS_CORGAREA"/>
                        </bd1:CORGAREA>
                        <!-- <xsl:choose>
                            <xsl:when test="$RS_CORGAREA = $CBTPAREA">
                                <bd1:CORGSCRCD></bd1:CORGSCRCD>
                            </xsl:when>
                            <xsl:otherwise>
                                <bd1:CORGSCRCD><xsl:value-of select="$RS_CORGSCRCD"/></bd1:CORGSCRCD>
                            </xsl:otherwise> 
                        </xsl:choose> -->
                        <bd1:CORGSCRCD>
                            <xsl:value-of select="$RS_CORGSCRCD"/>
                        </bd1:CORGSCRCD>
                        <bd1:COTPTYPE></bd1:COTPTYPE>

                        <bd1:CPACKTYPE>L</bd1:CPACKTYPE>

                        <bd1:CPREFTM></bd1:CPREFTM>
                        <bd1:CPRINTERTYPE></bd1:CPRINTERTYPE>
                        <bd1:CPRINTFLAG></bd1:CPRINTFLAG>

                        <bd1:CPRODCODE>A</bd1:CPRODCODE>

                        <bd1:CPRODFEATURE></bd1:CPRODFEATURE>

                        <bd1:CPRODTYPE>2</bd1:CPRODTYPE>

                        <bd1:CPUMODE></bd1:CPUMODE>

                        <bd1:CPUTIME>
                            <xsl:value-of select="format-time(current-time(),'[H01][m01]')"/>
                        </bd1:CPUTIME>

                        <bd1:CPUTYPE>OE</bd1:CPUTYPE>

                        <bd1:CREFNO1></bd1:CREFNO1>
                        <bd1:CREFNO2></bd1:CREFNO2>
                        <bd1:CREFNO3></bd1:CREFNO3>

                        <bd1:CREVPU>Y</bd1:CREVPU>

                        <bd1:CRFDCOMPNM></bd1:CRFDCOMPNM>
                        <bd1:CRTOADR1></bd1:CRTOADR1>
                        <bd1:CRTOADR2></bd1:CRTOADR2>

                        <bd1:CRTOADR3></bd1:CRTOADR3>
                        <bd1:CRTOADRDT></bd1:CRTOADRDT>
                        <bd1:CRTOCONTNM></bd1:CRTOCONTNM>
                        <bd1:CRTOLAT></bd1:CRTOLAT>
                        <bd1:CRTOLON></bd1:CRTOLON>
                        <bd1:CRTOMOB></bd1:CRTOMOB>
                        <bd1:CRTOMOBMAS></bd1:CRTOMOBMAS>
                        <bd1:CRTOPIN></bd1:CRTOPIN>
                        <bd1:CRTOTEL></bd1:CRTOTEL>

                        <bd1:CSENDER>
                            <xsl:value-of select="substring($CCUSTNAME,0,21)"/>
                        </bd1:CSENDER>
                        <bd1:CSPLINST>
                            <xsl:value-of
                                    select="concat('Customer Grading:',/LinkedHashMap/quote/customer/trusted_buyer_rating)"/>
                        </bd1:CSPLINST>
                        <bd1:CSTATECODE></bd1:CSTATECODE>
                        <bd1:CSUBCODE></bd1:CSUBCODE>
                        <bd1:DPUDATE>
                            <xsl:value-of select="format-date(current-date(),'[Y0001]-[M01]-[D01]')"/>
                        </bd1:DPUDATE>
                        <bd1:ISPUREGISTERED>Y</bd1:ISPUREGISTERED>
                        <bd1:ISREVPICKUP>Y</bd1:ISREVPICKUP>
                        <!-- <xsl:choose>
                            <xsl:when test="$RS_CORGAREA = $CBTPAREA">
                                <bd1:ISTOPAY>N</bd1:ISTOPAY>
                            </xsl:when>
                            <xsl:otherwise>
                                <bd1:ISTOPAY>Y</bd1:ISTOPAY>
                            </xsl:otherwise>
                        </xsl:choose> -->
                        <bd1:ISTOPAY>Y</bd1:ISTOPAY>
                        <bd1:ItemDetails>
                            <xsl:for-each select="/LinkedHashMap/quote/items">
                                <bd1:AWBItemDetails>
                                    <bd:Handheld></bd:Handheld>
                                    <bd:Location></bd:Location>
                                    <bd:LoginId></bd:LoginId>
                                    <bd1:BVALIDEBN></bd1:BVALIDEBN>
                                    <bd1:CAWBNO></bd1:CAWBNO>
                                    <bd1:CCNTRYORG></bd1:CCNTRYORG>
                                    <bd1:CDOCTYPE></bd1:CDOCTYPE>
                                    <bd1:CGSTSELL></bd1:CGSTSELL>
                                    <bd1:CHSCODE></bd1:CHSCODE>
                                    <bd1:CINSTRUCT></bd1:CINSTRUCT>
                                    <bd1:CINVNO></bd1:CINVNO>
                                    <bd1:CITEMID>
                                        <xsl:value-of select="id"/>
                                    </bd1:CITEMID>
                                    <bd1:CITEMNAME>
                                        <xsl:value-of select="line_of_business"/>
                                    </bd1:CITEMNAME>
                                    <bd1:CPCSID></bd1:CPCSID>
                                    <bd1:CPOSUPP></bd1:CPOSUPP>
                                    <bd1:CPRODDESC1>
                                        <xsl:value-of select="name"/>
                                    </bd1:CPRODDESC1>
                                    <bd1:NITEMVAL>
                                        <xsl:value-of select="$NDCLRDVAL"/>
                                    </bd1:NITEMVAL>
                                    <bd1:CPRODDESC2></bd1:CPRODDESC2>
                                    <bd1:CRETREASON></bd1:CRETREASON>
                                    <bd1:CSELLNAME></bd1:CSELLNAME>
                                    <bd1:CSKUNUM></bd1:CSKUNUM>
                                    <bd1:CSPLYTYPE></bd1:CSPLYTYPE>
                                    <bd1:CSUBPROD1></bd1:CSUBPROD1>
                                    <bd1:CSUBPROD2></bd1:CSUBPROD2>
                                    <bd1:CSUBPROD3></bd1:CSUBPROD3>
                                    <bd1:CSUBPROD4></bd1:CSUBPROD4>
                                </bd1:AWBItemDetails>
                            </xsl:for-each>
                        </bd1:ItemDetails>

                        <bd1:ItemQCDetails>
                            <xsl:for-each select="/LinkedHashMap/quote/items">
                                <xsl:variable name="itemid">
                                    <xsl:value-of select="id"/>
                                </xsl:variable>
                                <xsl:for-each select="/LinkedHashMap/quote/items/questions">
<!--                                    <xsl:if test="answer_value != 2">-->
                                        <bd1:AWBItemQCDetails>
                                            <xsl:variable name="quesid">
                                                <xsl:value-of select="question_id"/>
                                            </xsl:variable>
                                            <xsl:variable name="answervalue">
                                                <xsl:value-of select="answer_value"/>
                                            </xsl:variable>
                                            <xsl:variable name="product_question"
                                                          select="../product_questions[id=$quesid]"/>
                                            <!--Optional:-->
                                            <bd:Handheld></bd:Handheld>
                                            <!--Optional:-->
                                            <bd:Location></bd:Location>
                                            <!--Optional:-->
                                            <bd:LoginId></bd:LoginId>
                                            <!--Optional:-->
                                            <bd1:CANSWER>
                                                <xsl:value-of
                                                        select="substring($product_question/answers[value=$answervalue]/text,1,1)"/>
                                            </bd1:CANSWER>
                                            <!--Optional:-->
                                            <bd1:CAWBNO></bd1:CAWBNO>
                                            <!--Optional:-->
                                            <bd1:CISMANDATE>Y</bd1:CISMANDATE>
                                            <!--Optional:-->
                                            <bd1:CITEMID>
                                                <xsl:value-of select="$itemid"/>
                                            </bd1:CITEMID>
                                            <!--Optional:-->
                                            <bd1:CQCDESC>
                                                <xsl:value-of select="$product_question/text"/>
                                            </bd1:CQCDESC>
                                            <!--Optional:-->
                                            <bd1:CQCID>
                                                <xsl:value-of select="$quesid"/>
                                            </bd1:CQCID>
                                            <!--Optional:-->
                                            <bd1:CVALUE></bd1:CVALUE>
                                        </bd1:AWBItemQCDetails>
<!--                                    </xsl:if>-->
                                </xsl:for-each>
<!--                                <xsl:if test="line_of_business='non_ios'">-->
                                <xsl:if test="/LinkedHashMap/quote/items/imei != ''">
                                    <bd1:AWBItemQCDetails>
                                        <!--Optional:-->
                                        <bd:Handheld></bd:Handheld>
                                        <!--Optional:-->
                                        <bd:Location></bd:Location>
                                        <!--Optional:-->
                                        <bd:LoginId></bd:LoginId>
                                        <!--Optional:-->
                                        <bd1:CANSWER>Y</bd1:CANSWER>
                                        <!--Optional:-->
                                        <bd1:CAWBNO></bd1:CAWBNO>
                                        <!--Optional:-->
                                        <bd1:CISMANDATE>Y</bd1:CISMANDATE>
                                        <!--Optional:-->
                                        <bd1:CITEMID>
                                            <xsl:value-of select="$itemid"/>
                                        </bd1:CITEMID>
                                        <!--Optional:-->
                                        <bd1:CQCDESC>
                                            <xsl:value-of
                                                    select="concat('Are the last 5 digits of IMEI ',substring(imei,string-length(imei)-4,string-length(imei)),'?')"></xsl:value-of>
                                        </bd1:CQCDESC>
                                        <!--Optional:-->
<!--                                        <bd1:CQCID>92455</bd1:CQCID>-->
                                        <bd1:CQCID>000001</bd1:CQCID>
                                        <!--Optional:-->
                                        <bd1:CVALUE></bd1:CVALUE>
                                    </bd1:AWBItemQCDetails>
<!--                                </xsl:if>-->
                                </xsl:if>
                            </xsl:for-each>
                        </bd1:ItemQCDetails>
                        <bd1:NACTWGT>1.00</bd1:NACTWGT>
                        <bd1:NCOLAMT>0.00</bd1:NCOLAMT>
                        <bd1:NCOMVAL>0.00</bd1:NCOMVAL>
                        <bd1:NDCLRDVAL>
                            <xsl:value-of select="format-number(number($NDCLRDVAL),'#.0')"/>
                        </bd1:NDCLRDVAL>
                        <bd1:NDEFERREDDELIVERYDAYS>0</bd1:NDEFERREDDELIVERYDAYS>
                        <bd1:NITEMCNT>1</bd1:NITEMCNT>
                        <bd1:NPCS>1</bd1:NPCS>
                        <bd1:NWEIGHT>1.0</bd1:NWEIGHT>
                        <bd1:ParcelShopCode></bd1:ParcelShopCode>
                    </tem:req>
                    <tem:forAllExtService></tem:forAllExtService>
                </tem:WaybillGeneration_Ext>
            </soapenv:Body>
        </soapenv:Envelope>
    </xsl:template>
</xsl:stylesheet>
