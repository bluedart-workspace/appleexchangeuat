<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:param name="CCUSTADR1"/>
    <xsl:param name="CCUSTADR2"/>
    <xsl:param name="CCUSTADR3"/>
    <xsl:param name="CCUSTCODE"/>
    <xsl:param name="CCUSTNAME"/>
    <xsl:param name="CCUSTPIN"/>
    <xsl:param name="CCUSTTEL"/>
    <xsl:param name="CPACKTYPE"/>
    <xsl:param name="CMODE"/>
    <xsl:param name="CSENDER"/>
    <xsl:param name="CEXCHAWB"/>
    <xsl:param name="CORGAREA"/>
    <xsl:param name="CRTOADR1"/>
    <xsl:param name="CRTOADR2"/>
    <xsl:param name="CRTOADR3"/>
    <xsl:param name="CRTOPIN"/>
    <xsl:param name="CRTOTEL"/>
    <xsl:template match="/">
        <xsl:variable name="AWBNO" select="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/AWBTracking"/>

        <soapenv:Envelope
                xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                xmlns:tem="http://tempuri.org/"
                xmlns:bd="http://schemas.datacontract.org/2004/07/BD.Entities.Common"
                xmlns:bd1="http://schemas.datacontract.org/2004/07/BD.Entities.Area">
            <soapenv:Header/>
            <soapenv:Body>
                <tem:WaybillGeneration_Ext>
                    <tem:req>
                        <bd1:CATTENTION>
                            <xsl:value-of select="substring(/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/ConsigneeName,0,31)"/>
                        </bd1:CATTENTION>
                        <bd1:CAWBNO>
                            <xsl:value-of select="$AWBNO"/>
                        </bd1:CAWBNO>
                        <bd1:CCMDTYCODE>014</bd1:CCMDTYCODE>
                        <xsl:if test="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/ConsigneeAddress1 != ''">
                            <bd1:CCNEEADR1>
                                <xsl:value-of select="substring(/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/ConsigneeAddress1,0,31)"/>
                            </bd1:CCNEEADR1>
                        </xsl:if>
                        <xsl:if test="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/ConsigneeAddress2 != ''">
                            <bd1:CCNEEADR2>
                                <xsl:value-of select="substring(/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/ConsigneeAddress2,0,31)"/>
                            </bd1:CCNEEADR2>
                        </xsl:if>

                        <xsl:if test="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/ConsigneeAddress3!=''">
                            <bd1:CCNEEADR3>
                                <xsl:value-of select="substring(/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/ConsigneeAddress3,0,31)"/>
                            </bd1:CCNEEADR3>
                        </xsl:if>
                        <xsl:if test="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/ConsigneeMobile!=''">
                            <bd1:CCNEEMOB>
                                <xsl:value-of select="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/ConsigneeMobile"/>
                            </bd1:CCNEEMOB>
                        </xsl:if>
                        <xsl:if test="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/ConsigneeName!=''">
                            <bd1:CCNEENAME>
                                <xsl:value-of select="substring(/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/ConsigneeName,0,31)"/>
                            </bd1:CCNEENAME>
                        </xsl:if>
                        <xsl:if test="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/ConsigneePincode!=''">
                            <bd1:CCNEEPIN>
                                <xsl:value-of select="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/ConsigneePincode"/>
                            </bd1:CCNEEPIN>
                        </xsl:if>
                        <bd1:CCNEETEL></bd1:CCNEETEL>
                        <bd1:CCRCRDREF>
                            <xsl:value-of select="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/DeliveryNumber"/>
                        </bd1:CCRCRDREF>
                        <bd1:CCTMNO></bd1:CCTMNO>
                        <xsl:if test="$CCUSTADR1 != ''">
                            <bd1:CCUSTADR1>
                                <xsl:value-of select="substring($CCUSTADR1,0,31)"/>
                            </bd1:CCUSTADR1>
                        </xsl:if>
                        <xsl:if test="$CCUSTADR2 != ''">
                            <bd1:CCUSTADR2>
                                <xsl:value-of select="substring($CCUSTADR2,0,31)"/>
                            </bd1:CCUSTADR2>
                        </xsl:if>
                        <xsl:if test="$CCUSTADR3 != ''">
                            <bd1:CCUSTADR3>
                                <xsl:value-of select="substring($CCUSTADR3,0,31)"/>
                            </bd1:CCUSTADR3>
                        </xsl:if>


                        <xsl:if test="$CCUSTCODE != ''">
                            <bd1:CCUSTCODE>
                                <xsl:value-of select="$CCUSTCODE"/>
                            </bd1:CCUSTCODE>
                        </xsl:if>
                        <xsl:if test="$CCUSTNAME != ''">
                            <bd1:CCUSTNAME>
                                <xsl:value-of select="substring($CCUSTNAME,0,31)"/>
                            </bd1:CCUSTNAME>
                        </xsl:if>
                        <xsl:if test="$CCUSTPIN != ''">
                            <bd1:CCUSTPIN>
                                <xsl:value-of select="$CCUSTPIN"/>
                            </bd1:CCUSTPIN>
                        </xsl:if>
                        <xsl:if test="$CCUSTTEL != ''">
                            <bd1:CCUSTTEL>
                                <xsl:value-of select="$CCUSTTEL"/>
                            </bd1:CCUSTTEL>
                        </xsl:if>
                        <bd1:CEXCHAWB>
                            <xsl:value-of select="$CEXCHAWB"/>
                        </bd1:CEXCHAWB>
                        <bd1:CMODE>
                            <xsl:value-of select="$CMODE"/>
                        </bd1:CMODE>
                        <bd1:CORGAREA>
                            <xsl:value-of select="$CORGAREA"/>
                        </bd1:CORGAREA>
                        <xsl:if test="$CPACKTYPE != ''">
                            <bd1:CPACKTYPE><xsl:value-of select="$CPACKTYPE"/></bd1:CPACKTYPE>
                        </xsl:if>
                        <bd1:CPRODCODE>A</bd1:CPRODCODE>
                        <bd1:CPRODTYPE>2</bd1:CPRODTYPE>
                        <bd1:CPUMODE>P</bd1:CPUMODE>
                        <bd1:CPUTIME><xsl:value-of select="format-time(current-time(),'[H01][m01]')"/></bd1:CPUTIME>
                        <xsl:choose>
                            <xsl:when test="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/TradeinFlag = 'Y'">
                                <bd1:CPUTYPE>E</bd1:CPUTYPE>
                            </xsl:when>
                            <xsl:otherwise>
                                <bd1:CPUTYPE></bd1:CPUTYPE>
                            </xsl:otherwise>
                        </xsl:choose>
                        <bd1:CREVPU>N</bd1:CREVPU>
                        <xsl:if test="$CRTOADR1 != ''">
                            <bd1:CRTOADR1>
                                <xsl:value-of select="substring($CRTOADR1,0,31)"/>
                            </bd1:CRTOADR1>
                        </xsl:if>
                        <xsl:if test="$CRTOADR2 != ''">
                            <bd1:CRTOADR2>
                                <xsl:value-of select="substring($CRTOADR2,0,31)"/>
                            </bd1:CRTOADR2>
                        </xsl:if>
                        <xsl:if test="$CRTOADR3 != ''">
                            <bd1:CRTOADR3>
                                <xsl:value-of select="substring($CRTOADR3,0,26)"/>
                            </bd1:CRTOADR3>
                        </xsl:if>
                        <xsl:if test="$CRTOPIN != ''">
                            <bd1:CRTOPIN>
                                <xsl:value-of select="$CRTOPIN"/>
                            </bd1:CRTOPIN>
                        </xsl:if>
                        <xsl:if test="$CRTOTEL != ''">
                            <bd1:CRTOTEL>
                                <xsl:value-of select="$CRTOTEL"/>
                            </bd1:CRTOTEL>
                        </xsl:if>
                        <bd1:CSENDER>
                            <xsl:value-of select="substring($CSENDER,0,21)"/>
                        </bd1:CSENDER>
                        <bd1:CSPLINST>
                            <xsl:value-of select="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/WebOrderNumber"/>
                        </bd1:CSPLINST>
                        <bd1:DPUDATE>
                            <xsl:value-of select="format-date(current-date(),'[Y0001]-[M01]-[D01]')"/>
                        </bd1:DPUDATE>
                        <bd1:Dimensions>
                            <xsl:for-each select="tokenize(/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/BoxDimensions,';')[not(. = '')]">
                                <bd1:DimensionEntity>
                                    <bd1:AwbNo>
                                        <xsl:value-of select="$AWBNO"/>
                                    </bd1:AwbNo>
                                    <bd1:Breadth>
                                        <xsl:value-of select="ceiling(number(tokenize(.,',')[2]))"/>
                                    </bd1:Breadth>
                                    <bd1:Height>
                                        <xsl:value-of select="ceiling(number(tokenize(.,',')[3]))"/>
                                    </bd1:Height>
                                    <bd1:Length>
                                        <xsl:value-of select="ceiling(number(tokenize(.,',')[1]))"/>
                                    </bd1:Length>
                                    <bd1:SlabWeight>1</bd1:SlabWeight>
                                </bd1:DimensionEntity>
                            </xsl:for-each>
                        </bd1:Dimensions>
                        <bd1:ItemDetails>
                            <xsl:for-each select="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/details">
                                <bd1:AWBItemDetails>
                                    <bd1:CAWBNO>
                                        <xsl:value-of select="$AWBNO"/>
                                    </bd1:CAWBNO>
                                    <bd1:CINVNO>
                                        <xsl:value-of select="InvoiceNumber"/>
                                    </bd1:CINVNO>
                                    <bd1:CITEMID>
                                        <xsl:value-of select="concat('L',position())"/>
                                    </bd1:CITEMID>
                                    <bd1:CITEMNAME>
                                        <xsl:value-of select="substring(CommodityDescription,0,31)"/>
                                    </bd1:CITEMNAME>
                                    <xsl:choose>
                                        <xsl:when test="EWBNumber=''">
                                            <bd1:NEAWBNO>0</bd1:NEAWBNO>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <bd1:NEAWBNO>
                                                <xsl:value-of select="EWBNumber"/>
                                            </bd1:NEAWBNO>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <bd1:NITEMVAL>0</bd1:NITEMVAL>
                                </bd1:AWBItemDetails>
                            </xsl:for-each>
                        </bd1:ItemDetails>
                        <bd1:NACTWGT>
                            <xsl:value-of select="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/WeightOfShipment"/>
                        </bd1:NACTWGT>
                        <xsl:choose>
                            <xsl:when test="number(/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/CODAmount)>0">
                                <bd1:NCOLAMT><xsl:value-of select="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/CODAmount"/></bd1:NCOLAMT>
                            </xsl:when>
                            <xsl:otherwise><bd1:NCOLAMT>0.00</bd1:NCOLAMT></xsl:otherwise>
                        </xsl:choose>
                        <bd1:NDCLRDVAL>
                            <xsl:value-of select="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/DeclaredValueOfForwardShipment"/>
                        </bd1:NDCLRDVAL>
                        <bd1:NDEFERREDDELIVERYDAYS>0</bd1:NDEFERREDDELIVERYDAYS>
                        <bd1:NITEMCNT>
                            <xsl:value-of select="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/LineItemCount"/>
                        </bd1:NITEMCNT>
                        <xsl:choose>
                            <xsl:when test="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/ExchangeAmount = ''">
                                <bd1:NPAYCASH>0</bd1:NPAYCASH>
                            </xsl:when>
                            <xsl:otherwise>
                                <bd1:NPAYCASH>
                                    <xsl:value-of select="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/ExchangeAmount"/>
                                </bd1:NPAYCASH>
                            </xsl:otherwise>
                        </xsl:choose>

                        <bd1:NPCS>
                            <xsl:value-of select="/LinkedHashMap/request/requestPayload/payloadDetails/interimManifest/header/NumberOfBoxesInShipment"/>
                        </bd1:NPCS>
                        <bd1:NPUTMSLOT>0</bd1:NPUTMSLOT>
                    </tem:req>
                    <tem:forAllExtService></tem:forAllExtService>
                </tem:WaybillGeneration_Ext>
            </soapenv:Body>
        </soapenv:Envelope>
    </xsl:template>
</xsl:stylesheet>
